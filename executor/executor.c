/*
executor sql dopytov
Copyright (C) 2013 Martin Minarik <minarik11@student.fiit.stuba.sk>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <signal.h>
#include <my_global.h>
#include <mysql.h>

#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include "libsql.h"
#include "util.h"

static int stop = 0;

void int_handler(int asdf) {
	stop = 1;
}

struct execution {
/*
id 	bigint(20) unsigned
query_id 	bigint(20) unsigned
sandbox_id 	bigint(20) unsigned
start 	timestamp
end 	timestamp
checksum 	varchar(128)
check_rows 	int(10) unsigned
check_cols 	int(10) unsigned
error_msg 	varchar(256)
error_code 	int(10) unsigned 
*/
	unsigned long query_id;
	char checksum[130];
	unsigned long check_rows;
	unsigned long check_cols;
	char error_msg[258];
	unsigned long error_code;

};

struct ulohy {
	unsigned char query[32][4160];
	long unsigned int query_id[32];
	long unsigned int n;
};

struct executor {
	MYSQL *spoj_queue;
	my_ulonglong my_evaluator_id;
	MYSQL *spoj_sandbox;
	char sandbox_checksum[512];
	struct ulohy plan;
};


/*
int
queue_xretrieve(MYSQL *spoj, my_ulonglong *queue_row_id, my_ulonglong leecher_id)
{

}
*/
int uzamkni_test_db(struct executor *e)
{
	const char *buf_query_lokuj =
		"FLUSH TABLES WITH READ LOCK;";

	if (0 != mysql_query(e->spoj_queue, buf_query_lokuj)) {
		my_sql_soft_err(e->spoj_queue);
		return -1;
	}
	return 0;
}

int odomkni_test_db(struct executor *e)
{
	const char *buf_query_lokuj =
		"UNLOCK TABLES;";

	if (0 != mysql_query(e->spoj_queue, buf_query_lokuj)) {
		my_sql_soft_err(e->spoj_queue);
		return -1;
	}
	return 0;
}


int sprav_checksum_sandboxu(struct executor *e)
{
	char checksum[64] = {0};
	char *queries4hashes[128] = {NULL};
	int tbl = 0, tbi = 0, rowi = 0;

	MYSQL_ROW row;
	MYSQL_RES *result;

	char *databaza=NULL;	
	char **q;

	int num_fields, num_rows;
	int x;

	char *tabledb_query =
	"set @db = (SELECT DATABASE());";
	char *tablecols_query =
	"SELECT `TABLE_NAME`, `COLUMN_NAME` "
	"FROM `information_schema`.`COLUMNS` "
	"WHERE `TABLE_SCHEMA` =  @db;";
	char *query_hash_tbl =
	"SELECT CRC32( GROUP_CONCAT( CRC32(`%s`)    ) ) AS suma FROM  `%s` WHERE 1;";
	char *query_hash_tbl_konec =
	", CRC32(`%s`)   ) ) AS suma FROM  `%s` WHERE 1;";

	if (0 != mysql_query(e->spoj_sandbox, tabledb_query)) {
		my_sql_soft_err(e->spoj_sandbox);
		return -1;
	}

	if (0 != mysql_query(e->spoj_sandbox, tablecols_query)) {
		my_sql_soft_err(e->spoj_sandbox);
		return -1;
	}


	result = mysql_store_result(e->spoj_sandbox);

	num_fields = mysql_num_fields(result);

	if (num_fields != 2)
		return -2;

	num_rows = mysql_num_rows(result);

	while (0 < num_rows--) {

	row = mysql_fetch_row(result);

	if ((row[0][0] == 't') && (row[0][1] == 'm') && (row[0][2] == 'p'))
		continue;

	if ((databaza == NULL) || (0!=strcmp(databaza, row[0]))) {
		queries4hashes[tbl] = calloc(1024, 1);
		tbi = tbl;
		tbl++;
		queries4hashes[tbl] = NULL;

		sprintf(queries4hashes[tbi], query_hash_tbl, row[1], row[0]);
		x = 39 + strlen(row[1]);
	} else {

		sprintf(&queries4hashes[tbi][x], query_hash_tbl_konec, row[1], row[0]);
		x += 12 + strlen(row[1]);
	}

	free(databaza);
	databaza=strdup(row[0]);

	}

	free(databaza);



	mysql_free_result(result);


	for (q = &queries4hashes[0]; *q ; q++) {

		if (0 != mysql_query(e->spoj_sandbox, *q)) {
			my_sql_soft_err(e->spoj_sandbox);
			return -1;
		}

		free(*q);
		*q = NULL;

		result = mysql_store_result(e->spoj_sandbox);

		num_fields = mysql_num_fields(result);

		if (num_fields != 1)
			return -2;

		num_rows = mysql_num_rows(result);

		if (num_rows != 1)
			return -2;

		row = mysql_fetch_row(result);

		snprintf(checksum, sizeof(checksum), "%s", row[0]);

		mysql_free_result(result);

		/* FIXME: spojit check sumy do jednoho */

	}

	sprintf(e->sandbox_checksum, "%s", checksum);

	return 0;
}

int zaeviduj_sa(struct executor *e)
{
	MYSQL_RES *result;
	char buf_bafer_eviduj[1024];
	char *buf_query_eviduj =
		"INSERT INTO `cphpeval1`.`sandbox` ( "
		"`id` , "
		"`db_name` , "
		"`db_pass` , "
		"`db_host` , "
		"`db_port` , "
		"`checksum` , "
		"`executor_mode` , "
		"`executor_label` , "
		"`executor_last_alive` "
		") "
		"VALUES ( "
		"NULL , 'sendbox1', NULL , NULL , NULL , '%s', 'async', "
		"'executor.c', CURRENT_TIMESTAMP "
		");";

	sprintf(buf_bafer_eviduj, buf_query_eviduj, e->sandbox_checksum);

	if (0 != mysql_query(e->spoj_queue, buf_bafer_eviduj)) {
		my_sql_soft_err(e->spoj_queue);
		return -1;
	}

	result = mysql_store_result(e->spoj_queue);
	if (result == 0) {
		e->my_evaluator_id = mysql_insert_id(e->spoj_queue);
	} else {
		e->my_evaluator_id = ~0;
		return -1;
	}

	mysql_free_result(result);

	return 0;
}

int odeviduj_sa(struct executor *e)
{
	char buf_bafer_eviduj[1024];
	char *buf_query_eviduj =
		"DELETE FROM `sandbox` "
		"WHERE `id` = '%"PRIu64"';";

	if (e->my_evaluator_id == ~0)
		return -2;

	sprintf(buf_bafer_eviduj, buf_query_eviduj, e->my_evaluator_id);

	if (0 != mysql_query(e->spoj_queue, buf_bafer_eviduj)) {
		my_sql_soft_err(e->spoj_queue);
		return -1;
	}

	return 0;
}

void konstruktor(struct executor *e)
{
	const char *a = "/home/m/Desktop/SKOLA/bak/bak_repo0/executor/login_qdb.txt";
	const char *b = "/home/m/Desktop/SKOLA/bak/bak_repo0/executor/login_tdb.txt";

	naconnectuj(&e->spoj_queue, a);
	naconnectuj(&e->spoj_sandbox, b);

	uzamkni_test_db(e);
	sprav_checksum_sandboxu(e);
	odomkni_test_db(e);

	zaeviduj_sa(e);
}

void destruktor(struct executor *e)
{
	odeviduj_sa(e);

	disconnectuj(e->spoj_queue);
	disconnectuj(e->spoj_sandbox);
}

void sanitize256(char string[256])
{
	int i;
	for (i = 0; i < 256; i++) {
		if (string[i] == 0)
			break;
		if (string[i] == '\'')
			string[i] = '"';
	}
}

int vykonaj_ulohu(struct execution *x, struct executor *e, char *query)
{
	MYSQL_RES *result;
/* DEBUG
	printf("|%s|\n", query);
*/
	if (0 != mysql_query(e->spoj_sandbox, query)) {

		x->check_rows = 0;
		x->check_cols = 0;

		x->error_code = mysql_errno(e->spoj_sandbox);
		x->error_msg[0] = 0;
		strncpy(x->error_msg, mysql_error(e->spoj_sandbox), 254);
		x->error_msg[255] = 0;
		sanitize256(x->error_msg);
/* DEBUG*/
		my_sql_soft_err(e->spoj_sandbox);
/**/

		mysql_use_result(e->spoj_sandbox);
		return -1;
	}

	result = mysql_store_result(e->spoj_sandbox);
	x->check_rows = mysql_affected_rows(e->spoj_sandbox);
	x->check_cols = mysql_field_count(e->spoj_sandbox);
	x->error_code = 0;
	x->error_msg[0] = 0;
	mysql_free_result(result);

	return 0;
}

void zabal_ulohu(struct executor *e, const char *id, const char *q)
{
	long unsigned int qid;
	char *qvzor = "CREATE TABLE tmp%020lu ENGINE=MEMORY (%.16304s)";

	sscanf(id, "%"PRIu64"", (long unsigned int *) &qid);
	sprintf(e->plan.query[e->plan.n], qvzor, qid, q);
	e->plan.query_id[e->plan.n]=qid;
	e->plan.n++;
}

int nahlas_ulohu(struct executor *e, struct execution *x)
{
	char buf_bafer_report[2048];

	my_ulonglong qid, sid, num_rows, num_fields, err_code;

	qid = x->query_id;
	sid = 1;
	num_rows = x->check_rows;
	num_fields = x->check_cols;
	err_code = x->error_code;

	const char *buf_query_my_report =
		"INSERT INTO `cphpeval1`.`execution` ( "
		"`id`, `query_id`, `sandbox_id`, `start`, `end`, `checksum`, "
		"`check_rows`, `check_cols`, `error_msg`, `error_code`"
		") "
		"VALUES ("
		"NULL , '%"PRIu64"', '%"PRIu64"', NOW(), CURRENT_TIMESTAMP, "
		"'%.16304s', '%"PRIu64"', '%"PRIu64"', '%.16304s', '%"PRIu64"'"
		");";

	sprintf(buf_bafer_report, buf_query_my_report, qid, sid, "cek",
		num_rows, num_fields, x->error_msg, err_code);


	if (0 != mysql_query(e->spoj_queue, buf_bafer_report)) {

		my_sql_soft_err(e->spoj_queue);

		return -3;
	}



	mysql_use_result(e->spoj_queue);

	return 0;
}

int vybav_ulohu(struct execution *x, struct executor *e)
{
	if (e->plan.n == 0)
		return -2;

	long unsigned int n = --e->plan.n;
	long unsigned int qid = e->plan.query_id[n];
	unsigned char *q = e->plan.query[n];

	int res = vykonaj_ulohu(x, e, q);

	x->query_id = qid;

	return 0;
}

int zober_ulohu(struct executor *e)
{
	unsigned long int eid = 1;/*e->my_evaluator_id*/
	unsigned long int i;
	MYSQL_ROW row;
	my_ulonglong num_rows, num_fields;
	MYSQL_RES *result;
	char buf_bafer_zober[1024];

	const char *buf_query_my_schedule =
		"SELECT `query`.`id`, `query`.`sql` "
		"FROM `query` "
		"LEFT JOIN `definition` ON `definition`.`id` = `query`.`definition_id` "
		"LEFT JOIN `execution` ON `query`.`id` = `execution`.`query_id` "
		"AND `execution`.`sandbox_id` = '%"PRIu64"' "
		"WHERE ( "
		"`definition`.`sandbox_id` = '%"PRIu64"'"
		"OR `definition`.`hidden_sandbox_id` = '%"PRIu64"'"
		") "
		"AND `execution`.`id` IS NULL "
		"GROUP BY `query`.`id` "
		"LIMIT 0 , 32;";


/*
	char *start_t = "START TRANSACTION;";
	char *buf_query_zober =
		"UPDATE `execution` "
		"SET `status` = 'RUN' "
		"WHERE `execution`.`sandbox_id` = '%"PRIu64"' "
		"AND `status` = 'WAIT' "
		"ORDER BY `id` ASC "
		"LIMIT 1;";
	char *buf_query_potvrd =
		"UPDATE `execution` "
		"SET `status` = 'DONE' "
		"WHERE `execution`.`sandbox_id` = '%"PRIu64"' "
		"AND `status` = 'RUN' "
		"ORDER BY `id` ASC "
		"LIMIT 1;";
	char *buf_query_zlyhaj =
		"UPDATE `execution` "
		"SET `status` = 'FAIL', `sql_error` = '%.256s' "
		"WHERE `execution`.`sandbox_id` = '%"PRIu64"' "
		"AND `status` = 'RUN' "
		"ORDER BY `id` ASC "
		"LIMIT 1;";
	char *buf_query_zisti =
		"SELECT `query_id` "
		"FROM `execution` "
		"WHERE `execution`.`sandbox_id` = '%"PRIu64"' "
		"AND `status` = 'RUN' "
		"ORDER BY `id` ASC "
		"LIMIT 1;";
	char *commit_t = "COMMIT;";
*/
	sprintf(buf_bafer_zober, buf_query_my_schedule,eid,eid,eid);

	if (0 != mysql_query(e->spoj_queue, buf_bafer_zober)) {
		my_sql_soft_err(e->spoj_queue);
		return -3;
	}

/*
	if (0 != mysql_query(e->spoj_queue, buf_bafer_zober)) {
		my_sql_soft_err(e->spoj_queue);
		return -5;
	}
*/

	result = mysql_store_result(e->spoj_queue);

	if (result == 0) {
		my_sql_soft_err(e->spoj_queue);
		return -6;
	}

	num_fields = mysql_num_fields(result);

	if (num_fields != 2) {
		mysql_free_result(result);
		return -7;
	}

	num_rows = mysql_num_rows(result);

	if (num_rows == 0) {
		mysql_free_result(result);
		return -8;
	}

	e->plan.n = 0;

	for (i = 0; i < num_rows; i++) {

		row = mysql_fetch_row(result);

		zabal_ulohu(e, row[0], row[1]);

	}

/*
	sscanf(row[0], "%"PRIu64"", (long unsigned int *) &query_id);
*/
	mysql_free_result(result);

/*
	e->aktualne_query_id = query_id;
*/
	return 0;
	/* teraz ideme vybavit ulohu */
#if 0
	switch (vybav_ulohu(e)) {
	case -2:
		return -1;

	case -1:/*pouzivatel spravil syntakticku chybu*/

		/* sanitizujeme retazec*/
		sanitize256(e->aktualna_chyba);
/*
		sprintf(buf_bafer_zober, buf_query_zlyhaj,
			e->aktualna_chyba, e->my_evaluator_id);
*/
	break;
	default:
/*
		sprintf(buf_bafer_zober, buf_query_potvrd, e->my_evaluator_id);
*/
	break;
	}

/*
	if (0 != mysql_query(e->spoj_queue, buf_bafer_zober)) {
		my_sql_soft_err(e->spoj_queue);
		return -4;
	}
*/
	mysql_use_result(e->spoj_queue);
	return 0;
#endif
}

void cakaj_na_notify(struct executor *e)
{
	/* FIXME: nespat cakat na semafore na signal */
	sleep(1);

	/* FIXME: alebo mozeme este cakat na zmazanie nejakeho suboru v /tmp/ :D */

	/*no alebo proste hociako to zosynchronizovat najlepsie je, ze takto to
	funguje, komu sa to nepaci, patche su vitane*/
}

int bezim(struct executor *e)
{
	return !stop;
}

int main(int argc, char **argv)
{
	struct execution x;
	struct executor e;
	memset(&e, 0, sizeof e);

	signal(SIGINT, int_handler);
	signal(SIGTERM, int_handler);

	konstruktor(&e);
	while (bezim(&e)) {
		const int ok = 0;
		if (zober_ulohu(&e) == ok)
			while (vybav_ulohu(&x, &e) == ok)
				nahlas_ulohu(&e, &x);
				
		cakaj_na_notify(&e);
	};
	destruktor(&e);

	return 0;
}
