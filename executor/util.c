/*
executor sql dopytov
Copyright (C) 2013 Martin Minarik <minarik11@student.fiit.stuba.sk>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdio.h>
#include <string.h>

#include "util.h"

void load_login_info(struct login_info *l, const char *loginfile)
{
	FILE *f = NULL;
	char key[128], val[128];
	f = fopen(loginfile, "r");
	if (f == NULL)
		return;

	memset(key, 0, sizeof key);
	memset(val, 0, sizeof val);

	while (2==fscanf(f, "%127s %127s", &key[0], &val[0])){
		if (0==strcmp(key, "serv")) {
			strcpy(l->serv, val);
		}
		if (0==strcmp(key, "db")) {
			strcpy(l->db, val);
		}
		if (0==strcmp(key, "user")) {
			strcpy(l->user, val);
		}
		if (0==strcmp(key, "pass")) {
			strcpy(l->pass, val);
		}

	}

	fclose(f);
}
