/*
executor sql dopytov
Copyright (C) 2013 Martin Minarik <minarik11@student.fiit.stuba.sk>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <my_global.h>
#include <mysql.h>

#include "util.h"
#include "libsql.h"

void my_sql_soft_err(MYSQL *spoj)
{
	printf("Chyba mysql %u: %s\n", mysql_errno(spoj), mysql_error(spoj));
}

void my_sql_err(MYSQL *spoj)
{
	my_sql_soft_err(spoj);
	exit(1);
}

void naconnectuj(MYSQL **spoj, const char *loginfile)
{

	struct login_info l;
	load_login_info(&l, loginfile);

	mysql_library_init(0, NULL, NULL);

	*spoj = mysql_init(NULL);

	if (*spoj == NULL)
		my_sql_err(*spoj);

	if (!mysql_real_connect(*spoj, l.serv, l.user, 
		l.pass, NULL, 0, NULL, 0))
		my_sql_err(*spoj);

	if (0 != mysql_select_db(*spoj, l.db))
		my_sql_err(*spoj);
}

void disconnectuj(MYSQL *spoj)
{
	mysql_close(spoj);
	mysql_library_end();
}
