CREATE TABLE IF NOT EXISTS `evaluator_v1_ev_evaluation` (
 `hint` text NOT NULL, 
 `error_message` text NOT NULL, 
 `evaluation_result` int(11) unsigned NOT NULL, 
 `new_evaluator_state` int(11) unsigned NOT NULL, 
 `old_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `evaluator_v1_rq_evaluate` (
 `author` int(11) unsigned NOT NULL, 
 `query` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

