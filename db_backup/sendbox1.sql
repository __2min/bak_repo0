-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 14, 2013 at 11:50 AM
-- Server version: 5.5.29
-- PHP Version: 5.4.6-1ubuntu1.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sendbox1`
--

-- --------------------------------------------------------

--
-- Table structure for table `laureati`
--

CREATE TABLE IF NOT EXISTS `laureati` (
  `id` int(11) NOT NULL,
  `rok` int(11) NOT NULL,
  `obor` enum('physics','chemistry','peace','hekerstvo') NOT NULL,
  `laureat` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laureati`
--

INSERT INTO `laureati` (`id`, `rok`, `obor`, `laureat`) VALUES
(0, 1901, 'physics', 'Wilhelm Röntgen'),
(1, 2014, 'chemistry', 'este'),
(2, 1337, 'hekerstvo', 'ja');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
