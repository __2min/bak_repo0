-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 20, 2013 at 10:00 AM
-- Server version: 5.5.29
-- PHP Version: 5.4.6-1ubuntu1.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cphpeval1`
--

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE IF NOT EXISTS `author` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nick` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `author`
--

INSERT INTO `author` (`id`, `nick`) VALUES
(1, 'root'),
(2, 'jmrkva');

-- --------------------------------------------------------

--
-- Table structure for table `definition`
--

CREATE TABLE IF NOT EXISTS `definition` (
  `id` bigint(20) unsigned NOT NULL,
  `parent_id` bigint(20) unsigned NOT NULL,
  `label` varchar(256) NOT NULL COMMENT 'nadpis',
  `text` text NOT NULL,
  `sandbox_id` bigint(20) unsigned NOT NULL,
  `hidden_sandbox_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hidden_sandbox_id` (`hidden_sandbox_id`),
  KEY `sandbox_id` (`sandbox_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `definition`
--

INSERT INTO `definition` (`id`, `parent_id`, `label`, `text`, `sandbox_id`, `hidden_sandbox_id`) VALUES
(0, 0, 'Main definition', 'Hello World', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `execution`
--

CREATE TABLE IF NOT EXISTS `execution` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `query_id` bigint(20) unsigned NOT NULL,
  `sandbox_id` bigint(20) unsigned NOT NULL,
  `start` timestamp NULL DEFAULT NULL,
  `end` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `checksum` varchar(128) NOT NULL,
  `check_rows` int(10) unsigned NOT NULL,
  `check_cols` int(10) unsigned NOT NULL,
  `error_msg` varchar(256) NOT NULL,
  `error_code` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sandbox_id` (`sandbox_id`),
  KEY `query_id` (`query_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `execution`
--

INSERT INTO `execution` (`id`, `query_id`, `sandbox_id`, `start`, `end`, `checksum`, `check_rows`, `check_cols`, `error_msg`, `error_code`) VALUES
(1, 1, 1, NULL, '2013-11-19 14:23:52', '65sadf45sad', 0, 0, '', 0),
(2, 1, 1, NULL, '2013-11-13 13:15:57', 'dfa6v45da6s46adsv5', 0, 0, '', 0),
(3, 1, 1, NULL, '2013-11-19 14:23:45', 'ffffffff', 0, 0, '', 0),
(4, 1, 1, NULL, '2013-11-19 14:23:52', 'fff', 0, 0, '', 0),
(5, 1, 1, NULL, '2013-11-19 14:23:52', 'aa', 0, 0, '', 0),
(6, 1, 1, NULL, '2013-11-19 14:23:52', 'aaa', 0, 0, '', 0),
(7, 1, 1, NULL, '2013-11-19 14:23:52', 'asdf', 0, 0, '', 0),
(8, 1, 1, NULL, '2013-11-19 14:23:52', 'sadasdfasdffdssdz', 0, 0, '', 0),
(9, 1, 1, NULL, '2013-11-19 14:23:52', 'no', 0, 0, '', 0),
(10, 1, 1, NULL, '2013-11-19 14:23:52', 'aaa', 0, 0, '', 0),
(11, 1, 1, NULL, '2013-11-19 14:23:52', 'aaa', 0, 0, '', 0),
(12, 1, 1, NULL, '2013-11-19 14:23:52', 'test', 0, 0, '', 0),
(13, 1, 1, NULL, '2013-11-19 14:23:52', 'aaa', 0, 0, '', 0),
(14, 1, 1, NULL, '2013-11-19 14:23:52', 'aaaa', 0, 0, '', 0),
(15, 1, 1, NULL, '2013-11-19 14:23:52', 'aaa', 0, 0, 'e->aktualna_chyba', 0),
(16, 1, 1, NULL, '2013-11-19 14:23:52', 'aaa', 0, 0, '', 0),
(17, 1, 1, NULL, '2013-11-19 14:23:52', 'aaa', 0, 0, '', 0),
(18, 1, 1, NULL, '2013-11-19 14:23:52', 'aaaaa', 0, 0, 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near "create table bbb (aaa varchar(30)) )" at line 1', 0),
(19, 1, 1, NULL, '2013-11-19 14:23:52', 'aaaaa', 0, 0, 'Unknown column "hekerstvo" in "where clause"', 0),
(20, 1, 1, NULL, '2013-11-19 14:23:52', 'aaaaaaa', 0, 0, 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near "create table bbb (aaa varchar(30)) )" at line 1', 0),
(21, 1, 1, NULL, '2013-11-19 14:23:52', 'aaaa', 0, 0, 'Unknown column "hekerstvo" in "where clause"', 0),
(22, 1, 1, NULL, '2013-11-19 14:23:52', 'fff', 0, 0, '', 0),
(23, 1, 1, NULL, '2013-11-19 14:23:52', 'hahahahaha', 0, 0, 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near "drop database sendbox1 )" at line 1', 0),
(24, 1, 1, NULL, '2013-11-19 14:23:52', 'ffffff', 0, 0, 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near "slect * from neviemsql )" at line 1', 0),
(25, 1, 1, NULL, '2013-11-19 14:23:52', 'wwww', 0, 0, '', 0),
(26, 1, 1, NULL, '2013-11-19 14:23:52', 'aaa', 0, 0, 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near "drop database sendbox1 )" at line 1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `query`
--

CREATE TABLE IF NOT EXISTS `query` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `purpose` enum('USERTRY','SOLUTION','TABLE','UMLCLASS') NOT NULL DEFAULT 'USERTRY',
  `author_id` bigint(20) unsigned NOT NULL,
  `sql` varchar(4096) NOT NULL,
  `definition_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`),
  KEY `definition_id` (`definition_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `query`
--

INSERT INTO `query` (`id`, `purpose`, `author_id`, `sql`, `definition_id`) VALUES
(1, 'SOLUTION', 1, 'select * from laureati where rok=1911', 0),
(2, 'USERTRY', 1, 'select laureat from laureati where rok < 1903 and obor=''chemistry''', 0),
(3, 'USERTRY', 1, 'select * from laureati where laureat like "curie"', 0),
(4, 'TABLE', 1, 'select * from laureati where 1 limit 0,5', 0),
(5, 'USERTRY', 1, 'select * from laureati where laureat like ''este''', 0),
(6, 'USERTRY', 1, 'select * from laureati where rok=1915;', 0),
(7, 'USERTRY', 1, 'select * from laureati where obor=hekerstvo', 0),
(8, 'USERTRY', 1, 'create table bbb (aaa varchar(30));', 0),
(9, 'USERTRY', 1, 'create table bbb (aaa varchar(30))', 0),
(10, 'USERTRY', 1, 'drop database sendbox1', 0),
(11, 'USERTRY', 2, 'slect * from neviemsql', 0),
(12, 'USERTRY', 2, 'SELECT * \nFROM  `laureati` \nWHERE  `obor` =  ''physics''\n', 0),
(13, 'UMLCLASS', 1, 'DESCRIBE laureati;', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sandbox`
--

CREATE TABLE IF NOT EXISTS `sandbox` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `db_name` varchar(256) NOT NULL,
  `db_pass` varchar(128) DEFAULT NULL COMMENT 'zatial nevyuzite',
  `db_host` varchar(128) DEFAULT NULL COMMENT 'zatial nevyuzite',
  `db_port` smallint(5) unsigned DEFAULT NULL COMMENT 'zatial nevyuzite',
  `checksum` varchar(64) DEFAULT NULL COMMENT 'low priority zatial sa nevyuziva',
  `executor_mode` enum('off','sync','async') NOT NULL DEFAULT 'off',
  `executor_label` varchar(128) NOT NULL,
  `executor_last_alive` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sandbox`
--

INSERT INTO `sandbox` (`id`, `db_name`, `db_pass`, `db_host`, `db_port`, `checksum`, `executor_mode`, `executor_label`, `executor_last_alive`) VALUES
(1, '', NULL, NULL, NULL, NULL, 'async', 'aaa123', '2013-11-19 12:28:56');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `definition`
--
ALTER TABLE `definition`
  ADD CONSTRAINT `definition_ibfk_1` FOREIGN KEY (`hidden_sandbox_id`) REFERENCES `sandbox` (`id`),
  ADD CONSTRAINT `definition_ibfk_2` FOREIGN KEY (`sandbox_id`) REFERENCES `sandbox` (`id`);

--
-- Constraints for table `execution`
--
ALTER TABLE `execution`
  ADD CONSTRAINT `execution_ibfk_1` FOREIGN KEY (`query_id`) REFERENCES `query` (`id`),
  ADD CONSTRAINT `execution_ibfk_2` FOREIGN KEY (`sandbox_id`) REFERENCES `sandbox` (`id`);

--
-- Constraints for table `query`
--
ALTER TABLE `query`
  ADD CONSTRAINT `query_ibfk_2` FOREIGN KEY (`definition_id`) REFERENCES `definition` (`id`),
  ADD CONSTRAINT `query_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
