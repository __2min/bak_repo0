class CreateSafenvs < ActiveRecord::Migration
  def change
    create_table :safenvs do |t|
      t.string :dbname
      t.string :rwrite
      t.string :schema_hash
      t.string :tables_hash

      t.timestamps
    end
  end
end
