class SafenvsController < ApplicationController
  before_action :set_safenv, only: [:show, :edit, :update, :destroy]

  # GET /safenvs
  # GET /safenvs.json
  def index
    @safenvs = Safenv.all
  end

  # GET /safenvs/1
  # GET /safenvs/1.json
  def show
  end

  # GET /safenvs/new
  def new
    @safenv = Safenv.new
  end

  # GET /safenvs/1/edit
  def edit
  end

  # POST /safenvs
  # POST /safenvs.json
  def create
    @safenv = Safenv.new(safenv_params)

    respond_to do |format|
      if @safenv.save
        format.html { redirect_to @safenv, notice: 'Safenv was successfully created.' }
        format.json { render action: 'show', status: :created, location: @safenv }
      else
        format.html { render action: 'new' }
        format.json { render json: @safenv.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /safenvs/1
  # PATCH/PUT /safenvs/1.json
  def update
    respond_to do |format|
      if @safenv.update(safenv_params)
        format.html { redirect_to @safenv, notice: 'Safenv was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @safenv.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /safenvs/1
  # DELETE /safenvs/1.json
  def destroy
    @safenv.destroy
    respond_to do |format|
      format.html { redirect_to safenvs_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_safenv
      @safenv = Safenv.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def safenv_params
      params.require(:safenv).permit(:dbname, :rwrite, :schema_hash, :tables_hash)
    end
end
