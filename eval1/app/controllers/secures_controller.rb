class SecuresController < ApplicationController
  before_action :set_secure, only: [:show, :edit, :update, :destroy]

  # GET /secures
  # GET /secures.json
  def index
    @secures = Secure.all
  end

  # GET /secures/1
  # GET /secures/1.json
  def show
  end

  # GET /secures/new
  def new
    @secure = Secure.new
  end

  # GET /secures/1/edit
  def edit
  end

  # POST /secures
  # POST /secures.json
  def create
    @secure = Secure.new(secure_params)

    respond_to do |format|
      if @secure.save
        format.html { redirect_to @secure, notice: 'Secure was successfully created.' }
        format.json { render action: 'show', status: :created, location: @secure }
      else
        format.html { render action: 'new' }
        format.json { render json: @secure.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /secures/1
  # PATCH/PUT /secures/1.json
  def update
    respond_to do |format|
      if @secure.update(secure_params)
        format.html { redirect_to @secure, notice: 'Secure was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @secure.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /secures/1
  # DELETE /secures/1.json
  def destroy
    @secure.destroy
    respond_to do |format|
      format.html { redirect_to secures_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_secure
      @secure = Secure.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def secure_params
      params.require(:secure).permit(:status, :what, :where)
    end
end
