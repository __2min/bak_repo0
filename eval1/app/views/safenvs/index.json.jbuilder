json.array!(@safenvs) do |safenv|
  json.extract! safenv, :dbname, :rwrite, :schema_hash, :tables_hash
  json.url safenv_url(safenv, format: :json)
end
