json.array!(@queries) do |query|
  json.extract! query, :sql
  json.url query_url(query, format: :json)
end
