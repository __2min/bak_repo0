json.array!(@secures) do |secure|
  json.extract! secure, :status, :what, :where
  json.url secure_url(secure, format: :json)
end
