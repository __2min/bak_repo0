require 'test_helper'

class SafenvsControllerTest < ActionController::TestCase
  setup do
    @safenv = safenvs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:safenvs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create safenv" do
    assert_difference('Safenv.count') do
      post :create, safenv: { dbname: @safenv.dbname, rwrite: @safenv.rwrite, schema_hash: @safenv.schema_hash, tables_hash: @safenv.tables_hash }
    end

    assert_redirected_to safenv_path(assigns(:safenv))
  end

  test "should show safenv" do
    get :show, id: @safenv
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @safenv
    assert_response :success
  end

  test "should update safenv" do
    patch :update, id: @safenv, safenv: { dbname: @safenv.dbname, rwrite: @safenv.rwrite, schema_hash: @safenv.schema_hash, tables_hash: @safenv.tables_hash }
    assert_redirected_to safenv_path(assigns(:safenv))
  end

  test "should destroy safenv" do
    assert_difference('Safenv.count', -1) do
      delete :destroy, id: @safenv
    end

    assert_redirected_to safenvs_path
  end
end
