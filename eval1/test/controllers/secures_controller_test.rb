require 'test_helper'

class SecuresControllerTest < ActionController::TestCase
  setup do
    @secure = secures(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:secures)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create secure" do
    assert_difference('Secure.count') do
      post :create, secure: { status: @secure.status, what: @secure.what, where: @secure.where }
    end

    assert_redirected_to secure_path(assigns(:secure))
  end

  test "should show secure" do
    get :show, id: @secure
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @secure
    assert_response :success
  end

  test "should update secure" do
    patch :update, id: @secure, secure: { status: @secure.status, what: @secure.what, where: @secure.where }
    assert_redirected_to secure_path(assigns(:secure))
  end

  test "should destroy secure" do
    assert_difference('Secure.count', -1) do
      delete :destroy, id: @secure
    end

    assert_redirected_to secures_path
  end
end
