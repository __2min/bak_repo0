<?php
class Securesult extends AppModel {
	var $name = 'Securesult';
	var $useTable = 'securesult';
	var $displayField = 'sql_error';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Query' => array(
			'className' => 'Query',
			'foreignKey' => 'query_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Safenviron' => array(
			'className' => 'Safenviron',
			'foreignKey' => 'safenviron_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Coolset' => array(
			'className' => 'Coolset',
			'foreignKey' => 'coolset_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
