<?php
class Coolset extends AppModel {
	var $name = 'Coolset';
	var $useTable = 'coolset';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'Securesult' => array(
			'className' => 'Securesult',
			'foreignKey' => 'coolset_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
