<?php
class SafenvironsController extends AppController {

	var $name = 'Safenvirons';

	function index() {
		$this->Safenviron->recursive = 0;
		$this->set('safenvirons', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid safenviron', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('safenviron', $this->Safenviron->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Safenviron->create();
			if ($this->Safenviron->save($this->data)) {
				$this->Session->setFlash(__('The safenviron has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The safenviron could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid safenviron', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Safenviron->save($this->data)) {
				$this->Session->setFlash(__('The safenviron has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The safenviron could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Safenviron->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for safenviron', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Safenviron->delete($id)) {
			$this->Session->setFlash(__('Safenviron deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Safenviron was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->Safenviron->recursive = 0;
		$this->set('safenvirons', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid safenviron', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('safenviron', $this->Safenviron->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Safenviron->create();
			if ($this->Safenviron->save($this->data)) {
				$this->Session->setFlash(__('The safenviron has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The safenviron could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid safenviron', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Safenviron->save($this->data)) {
				$this->Session->setFlash(__('The safenviron has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The safenviron could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Safenviron->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for safenviron', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Safenviron->delete($id)) {
			$this->Session->setFlash(__('Safenviron deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Safenviron was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
