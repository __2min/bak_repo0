<?php
class SecuresultsController extends AppController {

	var $name = 'Securesults';

	function index() {
		$this->Securesult->recursive = 0;
		$this->set('securesults', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid securesult', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('securesult', $this->Securesult->read(null, $id));

		$qid = $this->Securesult->data['Securesult']['query_id'];

		$table = array();

		$link = mysqli_connect("localhost","cphpeval",
					"5B8CYq!XGtAZQrHu","sendbox1");
		if ($link) {
			$format = "SELECT * FROM tmp%020u where 1";

			$query = sprintf($format, $qid);

			$result = mysqli_query($link, $query);

			if (!is_bool($result)) {
				while($row = mysqli_fetch_array($result)) {
					$table[] = $row;
				}
			}

		}

		if (!empty($table))
			$this->set('table', $table);
	}

	function add() {
		if (!empty($this->data)) {
			$this->Securesult->create();
			if ($this->Securesult->save($this->data)) {
				$this->Session->setFlash(__('The securesult has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The securesult could not be saved. Please, try again.', true));
			}
		}
		$queries = $this->Securesult->Query->find('list');
		$safenvirons = $this->Securesult->Safenviron->find('list');
		$coolsets = $this->Securesult->Coolset->find('list');
		$this->set(compact('queries', 'safenvirons', 'coolsets'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid securesult', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Securesult->save($this->data)) {
				$this->Session->setFlash(__('The securesult has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The securesult could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Securesult->read(null, $id);
		}
		$queries = $this->Securesult->Query->find('list');
		$safenvirons = $this->Securesult->Safenviron->find('list');
		$coolsets = $this->Securesult->Coolset->find('list');
		$this->set(compact('queries', 'safenvirons', 'coolsets'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for securesult', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Securesult->delete($id)) {
			$this->Session->setFlash(__('Securesult deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Securesult was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->Securesult->recursive = 0;
		$this->set('securesults', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid securesult', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('securesult', $this->Securesult->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Securesult->create();
			if ($this->Securesult->save($this->data)) {
				$this->Session->setFlash(__('The securesult has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The securesult could not be saved. Please, try again.', true));
			}
		}
		$queries = $this->Securesult->Query->find('list');
		$safenvirons = $this->Securesult->Safenviron->find('list');
		$coolsets = $this->Securesult->Coolset->find('list');
		$this->set(compact('queries', 'safenvirons', 'coolsets'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid securesult', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Securesult->save($this->data)) {
				$this->Session->setFlash(__('The securesult has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The securesult could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Securesult->read(null, $id);
		}
		$queries = $this->Securesult->Query->find('list');
		$safenvirons = $this->Securesult->Safenviron->find('list');
		$coolsets = $this->Securesult->Coolset->find('list');
		$this->set(compact('queries', 'safenvirons', 'coolsets'));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for securesult', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Securesult->delete($id)) {
			$this->Session->setFlash(__('Securesult deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Securesult was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
