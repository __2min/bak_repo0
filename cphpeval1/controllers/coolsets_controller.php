<?php
class CoolsetsController extends AppController {

	var $name = 'Coolsets';

	function index() {
		$this->Coolset->recursive = 0;
		$this->set('coolsets', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid coolset', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('coolset', $this->Coolset->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Coolset->create();
			if ($this->Coolset->save($this->data)) {
				$this->Session->setFlash(__('The coolset has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coolset could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid coolset', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Coolset->save($this->data)) {
				$this->Session->setFlash(__('The coolset has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coolset could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Coolset->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for coolset', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Coolset->delete($id)) {
			$this->Session->setFlash(__('Coolset deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Coolset was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
