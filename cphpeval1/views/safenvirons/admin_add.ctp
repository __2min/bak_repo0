<div class="safenvirons form">
<?php echo $this->Form->create('Safenviron');?>
	<fieldset>
		<legend><?php __('Admin Add Safenviron'); ?></legend>
	<?php
		echo $this->Form->input('mode');
		echo $this->Form->input('sandbox_hash');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Safenvirons', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Securesults', true), array('controller' => 'securesults', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Securesult', true), array('controller' => 'securesults', 'action' => 'add')); ?> </li>
	</ul>
</div>