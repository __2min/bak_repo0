<div class="safenvirons view">
<h2><?php  __('Safenviron');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $safenviron['Safenviron']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Mode'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $safenviron['Safenviron']['mode']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Sandbox Hash'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $safenviron['Safenviron']['sandbox_hash']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Safenviron', true), array('action' => 'edit', $safenviron['Safenviron']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Safenviron', true), array('action' => 'delete', $safenviron['Safenviron']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $safenviron['Safenviron']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Safenvirons', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Safenviron', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Securesults', true), array('controller' => 'securesults', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Securesult', true), array('controller' => 'securesults', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Securesults');?></h3>
	<?php if (!empty($safenviron['Securesult'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Query Id'); ?></th>
		<th><?php __('Safenviron Id'); ?></th>
		<th><?php __('End'); ?></th>
		<th><?php __('Coolset Id'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Hash'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($safenviron['Securesult'] as $securesult):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $securesult['id'];?></td>
			<td><?php echo $securesult['query_id'];?></td>
			<td><?php echo $securesult['safenviron_id'];?></td>
			<td><?php echo $securesult['end'];?></td>
			<td><?php echo $securesult['coolset_id'];?></td>
			<td><?php echo $securesult['status'];?></td>
			<td><?php echo $securesult['hash'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'securesults', 'action' => 'view', $securesult['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'securesults', 'action' => 'edit', $securesult['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'securesults', 'action' => 'delete', $securesult['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $securesult['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Securesult', true), array('controller' => 'securesults', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
