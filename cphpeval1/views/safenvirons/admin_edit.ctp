<div class="safenvirons form">
<?php echo $this->Form->create('Safenviron');?>
	<fieldset>
		<legend><?php __('Admin Edit Safenviron'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('mode');
		echo $this->Form->input('sandbox_hash');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Safenviron.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Safenviron.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Safenvirons', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Securesults', true), array('controller' => 'securesults', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Securesult', true), array('controller' => 'securesults', 'action' => 'add')); ?> </li>
	</ul>
</div>