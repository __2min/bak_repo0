<div class="safenvirons index">
	<h2><?php __('Safenvirons');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('mode');?></th>
			<th><?php echo $this->Paginator->sort('sandbox_hash');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($safenvirons as $safenviron):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $safenviron['Safenviron']['id']; ?>&nbsp;</td>
		<td><?php echo $safenviron['Safenviron']['mode']; ?>&nbsp;</td>
		<td><?php echo $safenviron['Safenviron']['sandbox_hash']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $safenviron['Safenviron']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $safenviron['Safenviron']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $safenviron['Safenviron']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $safenviron['Safenviron']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Safenviron', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Securesults', true), array('controller' => 'securesults', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Securesult', true), array('controller' => 'securesults', 'action' => 'add')); ?> </li>
	</ul>
</div>