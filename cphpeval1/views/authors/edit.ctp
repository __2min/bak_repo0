<div class="authors form">
<?php echo $this->Form->create('Author');?>
	<fieldset>
		<legend><?php __('Edit Author'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('nick');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Author.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Author.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Authors', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Queries', true), array('controller' => 'queries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Query', true), array('controller' => 'queries', 'action' => 'add')); ?> </li>
	</ul>
</div>