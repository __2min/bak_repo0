<div class="authors view">
<h2><?php  __('Author');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $author['Author']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Nick'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $author['Author']['nick']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Author', true), array('action' => 'edit', $author['Author']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Author', true), array('action' => 'delete', $author['Author']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $author['Author']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Authors', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Author', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Queries', true), array('controller' => 'queries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Query', true), array('controller' => 'queries', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Queries');?></h3>
	<?php if (!empty($author['Query'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Author Id'); ?></th>
		<th><?php __('Start'); ?></th>
		<th><?php __('Sql'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($author['Query'] as $query):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $query['id'];?></td>
			<td><?php echo $query['author_id'];?></td>
			<td><?php echo $query['start'];?></td>
			<td><?php echo $query['sql'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'queries', 'action' => 'view', $query['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'queries', 'action' => 'edit', $query['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'queries', 'action' => 'delete', $query['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $query['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Query', true), array('controller' => 'queries', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
