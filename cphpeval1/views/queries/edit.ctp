<div class="queries form">
<?php echo $this->Form->create('Query');?>
	<fieldset>
		<legend><?php __('Edit Query'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('author_id');
		echo $this->Form->input('start');
		echo $this->Form->input('sql');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Query.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Query.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Queries', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Authors', true), array('controller' => 'authors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Author', true), array('controller' => 'authors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Securesults', true), array('controller' => 'securesults', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Securesult', true), array('controller' => 'securesults', 'action' => 'add')); ?> </li>
	</ul>
</div>