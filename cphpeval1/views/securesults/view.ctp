<div class="securesults view">
<h2><?php  __('Securesult');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $securesult['Securesult']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Query'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($securesult['Query']['sql'], array('controller' => 'queries', 'action' => 'view', $securesult['Query']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Safenviron'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($securesult['Safenviron']['id'], array('controller' => 'safenvirons', 'action' => 'view', $securesult['Safenviron']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('End'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $securesult['Securesult']['end']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Coolset'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($securesult['Coolset']['id'], array('controller' => 'coolsets', 'action' => 'view', $securesult['Coolset']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Status'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $securesult['Securesult']['status']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Hash'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $securesult['Securesult']['hash']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Sql Error'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $securesult['Securesult']['sql_error']; ?>
			&nbsp;
		</dd>
	</dl>
<?php if (isset($table)) { ?>
<h2><?php  __('Table');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>

	<?php $i = 0; $class = ' class="altrow"';?>
	<?php foreach($table[0] as $col=>$asdf) { ?>
	<?php if (is_int($col)) continue; ?>
		<th<?php if ($i % 2 == 0) echo $class;?>><?php echo $col; ?></th>
	<?php } ?>
	</tr>
	<?php
	$i = 0;
	foreach ($table as $row):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr>
	<?php   foreach ($row as $id=>$cell): ?>
	<?php if (is_int($id)) continue; ?>
	<td>
	<?php print_r($cell); ?>
	</td>
	<?php endforeach; ?>
	</tr>
<?php endforeach; ?>
	</table>
<?php } ?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Securesult', true), array('action' => 'edit', $securesult['Securesult']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Securesult', true), array('action' => 'delete', $securesult['Securesult']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $securesult['Securesult']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Securesults', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Securesult', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Queries', true), array('controller' => 'queries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Query', true), array('controller' => 'queries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Safenvirons', true), array('controller' => 'safenvirons', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Safenviron', true), array('controller' => 'safenvirons', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Coolsets', true), array('controller' => 'coolsets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Coolset', true), array('controller' => 'coolsets', 'action' => 'add')); ?> </li>
	</ul>
</div>
