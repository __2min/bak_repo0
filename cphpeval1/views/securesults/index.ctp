<div class="securesults index">
	<h2><?php __('Securesults');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('query_id');?></th>
			<th><?php echo $this->Paginator->sort('safenviron_id');?></th>
			<th><?php echo $this->Paginator->sort('end');?></th>
			<th><?php echo $this->Paginator->sort('coolset_id');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th><?php echo $this->Paginator->sort('hash');?></th>
			<th><?php echo $this->Paginator->sort('sql_error');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($securesults as $securesult):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $securesult['Securesult']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($securesult['Query']['sql'], array('controller' => 'queries', 'action' => 'view', $securesult['Query']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($securesult['Safenviron']['id'], array('controller' => 'safenvirons', 'action' => 'view', $securesult['Safenviron']['id'])); ?>
		</td>
		<td><?php echo $securesult['Securesult']['end']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($securesult['Coolset']['id'], array('controller' => 'coolsets', 'action' => 'view', $securesult['Coolset']['id'])); ?>
		</td>
		<td><?php echo $securesult['Securesult']['status']; ?>&nbsp;</td>
		<td><?php echo $securesult['Securesult']['hash']; ?>&nbsp;</td>
		<td><?php echo $securesult['Securesult']['sql_error']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $securesult['Securesult']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $securesult['Securesult']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $securesult['Securesult']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $securesult['Securesult']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Securesult', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Queries', true), array('controller' => 'queries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Query', true), array('controller' => 'queries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Safenvirons', true), array('controller' => 'safenvirons', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Safenviron', true), array('controller' => 'safenvirons', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Coolsets', true), array('controller' => 'coolsets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Coolset', true), array('controller' => 'coolsets', 'action' => 'add')); ?> </li>
	</ul>
</div>