<div class="securesults form">
<?php echo $this->Form->create('Securesult');?>
	<fieldset>
		<legend><?php __('Admin Add Securesult'); ?></legend>
	<?php
		echo $this->Form->input('query_id');
		echo $this->Form->input('safenviron_id');
		echo $this->Form->input('end');
		echo $this->Form->input('coolset_id');
		echo $this->Form->input('status');
		echo $this->Form->input('hash');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Securesults', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Queries', true), array('controller' => 'queries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Query', true), array('controller' => 'queries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Safenvirons', true), array('controller' => 'safenvirons', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Safenviron', true), array('controller' => 'safenvirons', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Coolsets', true), array('controller' => 'coolsets', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Coolset', true), array('controller' => 'coolsets', 'action' => 'add')); ?> </li>
	</ul>
</div>