<div class="coolsets form">
<?php echo $this->Form->create('Coolset');?>
	<fieldset>
		<legend><?php __('Edit Coolset'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('style');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Coolset.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Coolset.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Coolsets', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Securesults', true), array('controller' => 'securesults', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Securesult', true), array('controller' => 'securesults', 'action' => 'add')); ?> </li>
	</ul>
</div>