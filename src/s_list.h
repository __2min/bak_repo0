#ifndef S_LIST_H
#define S_LIST_H

struct s_list {
	struct s_list *pred;
	struct s_list *nasl;
};

#ifdef __GNUC__
#define s_ct_of(a, b, c)						\
	(__typeof__(b))((char *)(a)	-				\
		 ((char *)&(b)->c - (char *)(b)))
#else
#define s_ct_of(ptr, b, c)						\
	(void *)((char *)(ptr)	-				        \
		 ((char *)&(b)->c - (char *)(b)))
#endif
#define s_list_for_each(a, b, c)					\
	for (a = 0, a = s_ct_of((b)->nasl, a, c);			\
	     &a->c != (b);						\
	     a = s_ct_of(a->c.nasl, a, c))

void
s_list_init(struct s_list *l);
void
s_list_insert(struct s_list *l, struct s_list *e);
void
s_list_remove(struct s_list *e);

#endif
