#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <expat.h>
#include <assert.h>
#include "s_list.h"

enum sql_col_typ {
SQL_T_UINT = 0,
SQL_T_VARCHAR_512 = 1,
SQL_T_STRING = 2
};

const char *sql_col_decl[] = {
"int(11) unsigned NOT NULL",
"varchar(512) NOT NULL",
"text NOT NULL",
NULL};

struct sql_databaza {
	char *nazov;
	struct s_list tabulky;
	struct s_list linka;
};

struct sql_tabulka {
	char *nazov;
	struct s_list stlpce;
	struct s_list linka;
};

struct sql_stlpec {
	char *nazov;
	enum sql_col_typ typ;
	struct s_list linka;
};

#define sql_db_max 32
#define sql_tbl_max 512
#define sql_col_max 1024
#define xml_iface_name_max 128
#define xml_attr_name_max 128

struct sql {
	struct sql_databaza db[sql_db_max];
	struct sql_tabulka tbl[sql_tbl_max];
	struct sql_stlpec col[sql_col_max];
	off_t db_alloc, tbl_alloc, col_alloc;
};

struct parser_user_data {
	struct sql *sql;
	enum sql_col_typ attr_type;
	char iface_name[xml_iface_name_max];
	char attr_name[xml_attr_name_max];
	off_t curr_tbl;
	unsigned int iface_version;
};

struct sql_databaza *off2db(struct sql *sql, off_t o)
{
	assert(o >= 0);
	assert(o < sql_db_max);
	return &sql->db[o];
}
struct sql_tabulka *off2tbl(struct sql *sql, off_t o)
{
	assert(o >= 0);
	assert(o < sql_tbl_max);
	return &sql->tbl[o];
}
struct sql_stlpec *off2col(struct sql *sql, off_t o)
{
	assert(o >= 0);
	assert(o < sql_col_max);
	return &sql->col[o];
}

off_t sql_db_init(struct sql *sql, const char *nazov)
{
	struct sql_databaza *db = &sql->db[sql->db_alloc];
	assert(sql->db_alloc < sql_db_max);
	s_list_init(&db->tabulky);
	s_list_init(&db->linka);
	db->nazov = strdup(nazov);
	return sql->db_alloc++;
}

off_t sql_tbl_init(struct sql *sql, off_t db_o, const char *nazov)
{
	struct sql_tabulka *tbl = &sql->tbl[sql->tbl_alloc];
	assert(sql->tbl_alloc < sql_tbl_max);
	s_list_init(&tbl->stlpce);
	s_list_init(&tbl->linka);
	s_list_insert(&off2db(sql, db_o)->tabulky, &tbl->linka);
	tbl->nazov = strdup(nazov);
	return sql->tbl_alloc++;
}

off_t sql_col_init(struct sql *sql, off_t tbl_o, const char *nazov, enum sql_col_typ typ)
{
	struct sql_stlpec *col = &sql->col[sql->col_alloc];
	assert(sql->col_alloc < sql_col_max);
	s_list_init(&col->linka);
	s_list_insert(&off2tbl(sql, tbl_o)->stlpce, &col->linka);
	col->nazov = strdup(nazov);
	col->typ = typ;
	return sql->col_alloc++;
}

void sql_release(struct sql *sql)
{
	off_t i;
	for (i = 0; i < sql->db_alloc; i++) {
		free(sql->db[i].nazov);
	}
	for (i = 0; i < sql->col_alloc; i++) {
		free(sql->col[i].nazov);
	}
	for (i = 0; i < sql->tbl_alloc; i++) {
		free(sql->tbl[i].nazov);
	}
	free(sql);
}


static void
end_tag(void *data, const char *tag_name)
{
	struct parser_user_data *d = data;
	if (0 == strcmp("request", tag_name)) {
		d->curr_tbl = ~0;
	} else
	if (0 == strcmp("event", tag_name)) {
		d->curr_tbl = ~0;
	} else
	if (0 == strcmp("arg", tag_name)) {
/*
		printf("mam konec arg %s %u\n", d->attr_name, d->attr_type);
*/
		sql_col_init(d->sql, d->curr_tbl, d->attr_name, d->attr_type);

	} else
	if (0 == strcmp("protocol", tag_name)) {
		int i;
		struct sql_databaza *db = &d->sql->db[0];
		struct sql_tabulka *tbl;
		struct sql_stlpec *col;
		s_list_for_each(tbl, &db->tabulky, linka) {
			char *tblname = tbl->nazov;

			printf("CREATE TABLE IF NOT EXISTS `%s` (", tblname);
		i = 0;
		s_list_for_each(col, &tbl->stlpce, linka) {
			if (i) {
				printf(", \n");
			} else {
				printf("\n");
			}
			printf(" `%s` %s", col->nazov, sql_col_decl[col->typ]);


			i++;
		}
			printf("\n) ENGINE=InnoDB DEFAULT CHARSET=latin1;\n\n");
		}


	}
/*

*/
}

enum tag_attrib {
ATTR_UNKNOWN = 0,
ATTR_NAME = 1,
ATTR_VERSION = 2,
ATTR_TYPE = 3,
};

static void
start_tag(void *data, const char *tag_name, const char **atts)
{
	struct parser_user_data *d = data;
	const char **i;
	char value;
	enum tag_attrib t;

	for (value = 0, i = atts; *i; i++, value = !value) {

		if (!value) {
			t = ATTR_UNKNOWN;
			if (0 == strcmp("name", *i)) {
				t = ATTR_NAME;
			}
			if (0 == strcmp("version", *i)) {
				t = ATTR_VERSION;
			}
			if (0 == strcmp("type", *i)) {
				t = ATTR_TYPE;
			}
		}

		if (0 == strcmp("interface", tag_name)) {
			if (value) {
				if (t == ATTR_NAME) {
					d->iface_name[xml_iface_name_max-1] = 0;
					strncpy(d->iface_name, *i, xml_iface_name_max-2);
				}
				if (t == ATTR_VERSION) {
					unsigned int n;
					sscanf(*i, "%u", &n);
					d->iface_version = n;
				}
			}
		} else
		if (0 == strcmp("request", tag_name)) {
			if (value) {
				if (t == ATTR_NAME) {
					char bafer[512];

					sprintf(bafer, "%s_v%u_rq_%s", d->iface_name, d->iface_version, *i);

					bafer[511] = 0;
					d->curr_tbl = sql_tbl_init(d->sql, 0, bafer);

				}
			}
		} else
		if (0 == strcmp("event", tag_name)) {
			if (value) {
				if (t == ATTR_NAME) {
					char bafer[512];

					sprintf(bafer, "%s_v%u_ev_%s", d->iface_name, d->iface_version, *i);

					bafer[511] = 0;
					d->curr_tbl = sql_tbl_init(d->sql, 0, bafer);

				}
			}
		} else
		if (0 == strcmp("arg", tag_name)) {
			if (value) {
				if (t == ATTR_NAME) {
					d->attr_name[xml_attr_name_max-1] = 0;
					strncpy(d->attr_name, *i, xml_attr_name_max-2);
				}
				if (t == ATTR_TYPE) {
					if (0 == strcmp("uint", *i)) {
						d->attr_type = SQL_T_UINT;
					}
					if (0 == strcmp("string", *i)) {
						d->attr_type = SQL_T_STRING;
					}
				}
			}
		}
	}
}

static void
character_data(void *data, const XML_Char *s, int len)
{
/*
	printf("mam char data [%s] velkosti %i \n", s, len);
*/
}

int inma(struct parser_user_data *data)
{
	const size_t xml_buffer_size = 4096;
	void *buffer;
	size_t len;
	void *parser = XML_ParserCreate(NULL);
	if (parser == NULL) {
		fprintf(stderr, "Chyba neda sa inicializovat parser\n");
		return -1;
	}


	XML_SetUserData(parser, data);

	XML_SetElementHandler(parser, start_tag, end_tag);
	XML_SetCharacterDataHandler(parser, character_data);

	do {
		buffer = XML_GetBuffer(parser, xml_buffer_size);
		len = fread(buffer, 1, xml_buffer_size, stdin);
		if (len < 0) {
			fprintf(stderr, "fread: %m\n");
			return -1;
		}
		XML_ParseBuffer(parser, len, len == 0);

	} while (len > 0);

	XML_ParserFree(parser);
/*
	fprintf(stderr, "som tu \n");
*/
}

int main(int argc, char *argv[])
{
	int rc;
	struct parser_user_data data;
	data.sql = calloc(1, sizeof(struct sql));
	if (data.sql == NULL) {
		fprintf(stderr, "Chyba neda sa alokovat data\n");
		return -1;
	}
	data.iface_name[0] = 0;
	data.attr_name[0] = 0;
	data.iface_version = 0;

	sql_db_init(data.sql, "aaa");

	rc = inma(&data);

	sql_release(data.sql);
	return rc;
}
