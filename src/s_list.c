#include "s_list.h"

#include <stdlib.h>

void
s_list_init(struct s_list *l)
{
	l->pred = l;
	l->nasl = l;
}

void
s_list_insert(struct s_list *l, struct s_list *e)
{
	e->pred = l;
	e->nasl = l->nasl;
	l->nasl = e;
	e->nasl->pred = e;
}

void
s_list_remove(struct s_list *e)
{
	e->pred->nasl = e->nasl;
	e->nasl->pred = e->pred;
	e->nasl = NULL;
	e->pred = NULL;
}
