json.array!(@definition) do |definition|
  json.extract! definition, 
  json.url definition_url(definition, format: :json)
end
