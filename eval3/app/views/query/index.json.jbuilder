json.array!(@query) do |query|
  json.extract! query, 
  json.url query_url(query, format: :json)
end
