module DefinitionHelper
	def markdown2html(text)
		opt = {
			:autolink => true,
			:space_after_headers => true,
			:no_intra_emphasis => true
		}
		markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, opt)
		markdown.render(text).html_safe
	end
end
