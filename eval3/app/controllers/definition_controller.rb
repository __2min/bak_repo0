class DefinitionController < ApplicationController
  before_action :set_definition, only: [:show, :edit, :update, :destroy]

  # GET /definition
  # GET /definition.json
  def index
    @definition = Definition.all
  end

  # GET /definition/1
  # GET /definition/1.json
  def show
	@definition = Definition.find_by_id(params[:id])
	@all_subdefinition = @definition.children.all
	@query_solution = Query.where(definition_id: params[:id],
					purpose: "SOLUTION");
	@query_table = Query.where(	definition_id: params[:id],
					purpose: "TABLE");
	@query_umlimage = Query.where(definition_id: params[:id],
					purpose: "UMLCLASS");
	@execution_solution = Execution.where(query_id: @query_solution.first.id)
	@execution_table = Execution.where(query_id: @query_table.first.id)
	@execution_umlimage = Execution.where(query_id: @query_umlimage.first.id)
	if nil == @query_solution
	@query_solution = []
	end
	if nil == @query_table
	@query_table = []
	end
	if nil == @query_umlimage
	@query_umlimage = []
	end
  end

  # GET /definition/new params[:id]
  def new
	@par = Definition.find_by_id(params[:id])
	if nil == @par
	@par = Definition.find_by_id("1")
	end
	@definition = Definition.new(	:parent_id => @par.id,
					:sandbox_id => @par.sandbox_id,
					:hidden_sandbox_id => @par.hidden_sandbox_id)

  end

  # GET /definition/1/edit
  def edit
	@definition = Definition.find(params[:id])
  end

  # POST /definition
  # POST /definition.json
  def create
    @definition = Definition.new(definition_params)

    respond_to do |format|
      if @definition.save
        format.html { redirect_to @definition, notice: 'Definition was successfully created.' }
        format.json { render action: 'show', status: :created, location: @definition }
      else
        format.html { render action: 'new' }
        format.json { render json: @definition.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /definition/1
  # PATCH/PUT /definition/1.json
  def update
    respond_to do |format|
      if @definition.update(definition_params)
        format.html { redirect_to @definition, notice: 'Definition was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @definition.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /definition/1
  # DELETE /definition/1.json
  def destroy
	if "1" != @definition.id
		@definition.destroy
	end
    respond_to do |format|
      format.html { redirect_to definition_index_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_definition
      @definition = Definition.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def definition_params
      params[:definition]
    end
end
