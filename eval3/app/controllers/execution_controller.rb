class ExecutionController < ApplicationController
  before_action :set_execution, only: [:show, :edit, :update, :destroy]

  # GET /execution
  # GET /execution.json
  def index
    @execution = Execution.all
  end

  # GET /execution/1
  # GET /execution/1.json
  def show
	get_tmp_table(@execution)
  end

  # GET /execution/new
  def new
    @execution = Execution.new
  end

  # GET /execution/1/edit
  def edit
  end

  # POST /execution
  # POST /execution.json
  def create
    @execution = Execution.new(execution_params)

    respond_to do |format|
      if @execution.save
        format.html { redirect_to @execution, notice: 'Execution was successfully created.' }
        format.json { render action: 'show', status: :created, location: @execution }
      else
        format.html { render action: 'new' }
        format.json { render json: @execution.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /execution/1
  # PATCH/PUT /execution/1.json
  def update
    respond_to do |format|
      if @execution.update(execution_params)
        format.html { redirect_to @execution, notice: 'Execution was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @execution.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /execution/1
  # DELETE /execution/1.json
  def destroy
    @execution.destroy
    respond_to do |format|
      format.html { redirect_to execution_index_url }
      format.json { head :no_content }
    end
  end

  private
	def get_tmp_table(execution)

		sufix = "%020d" % execution.query.id

		tabulka = "tmp"+sufix

		#p(tabulka)

		ActiveRecord::Base.uncached() do
			begin
			Tmp.table_name_suffix = sufix;
			@tmptable = Tmp.find_by_sql("select * from `"+tabulka+"`;")
			rescue
			@tmptable = nil;
			end
		end
	end

    # Use callbacks to share common setup or constraints between actions.
    def set_execution
      @execution = Execution.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def execution_params
      params[:execution]
    end
end
