class QueryController < ApplicationController
  before_action :set_query, only: [:show, :edit, :update, :destroy]

  # GET /query
  # GET /query.json
  def index
    @query = Query.all
  end

  # GET /query/1
  # GET /query/1.json
  def show
  end

  # GET /query/new
  def new
    @query = Query.new
  end

  # GET /query/1/edit
  def edit
  end

  # POST /query
  # POST /query.json
  def create
    @query = Query.new(query_params)

    respond_to do |format|
      if @query.save
        format.html { redirect_to @query, notice: 'Query was successfully created.' }
        format.json { render action: 'show', status: :created, location: @query }
      else
        format.html { render action: 'new' }
        format.json { render json: @query.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /query/1
  # PATCH/PUT /query/1.json
  def update
    respond_to do |format|
      if @query.update(query_params)
        format.html { redirect_to @query, notice: 'Query was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @query.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /query/1
  # DELETE /query/1.json
  def destroy
    @query.destroy
    respond_to do |format|
      format.html { redirect_to query_index_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_query
      @query = Query.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def query_params
      params[:query]
    end
end
