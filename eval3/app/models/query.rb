class Query < ActiveRecord::Base
	has_one :author
	has_one :definition
	has_many :execution

	attr_accessible :sql, :author_id, :purpose, :definition_id
end
