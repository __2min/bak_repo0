class Execution < ActiveRecord::Base
	belongs_to :definition, class_name: "Definition"
	belongs_to :query, class_name: "Query"
end
