class Definition < ActiveRecord::Base
	validates_presence_of :text, :sandbox_id, :hidden_sandbox_id, :label

	has_many :children, :class_name => "Definition", :foreign_key => "parent_id"
	belongs_to :parent, :class_name => "Definition"
	has_one :sandbox
	has_one :hidden_sandbox
	has_many :query

	attr_accessible :parent, :parent_id, :text, :sandbox_id, :hidden_sandbox_id, :label

	accepts_nested_attributes_for :parent

#/*
=begin
napriklad takto

 has_many :bb,
 :class_name => "XxxYyy",
 :foreign_key => :xx_id,
 :dependent => :destroy
 has_many :aa,
 :through => :bb,
 :source => :aa
=end
#*/


end
