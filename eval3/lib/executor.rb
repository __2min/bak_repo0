require '../config/environment.rb'

class Executor


   def initialize(name)
      @name = name.capitalize
   end


   def sayHi
      puts "Hello #{@name}!"
   end
end

main = Executor.new("World")
main.sayHi
