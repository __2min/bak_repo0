# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_ToDo_session',
  :secret      => 'ec007a68eeb07cfa1bcbcf20c5ba25d13bf9aa3bdf85355e5f846625960b065bf209b14248649e39a31b17b3b11c05c96d547b80d727b5aa4cd91f74f1168242'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
